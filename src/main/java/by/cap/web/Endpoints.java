package by.cap.web;

/**
 * API endpoints and its parts.
 */
class Endpoints {

    final static String VERSION = "/v1";
    final static String ENDPOINT_USERS = "/users";
    final static String ENDPOINT_ACCOUNTS = "/accounts";
    final static String ID_PARAM = ":id";
    final static String ENDPOINT_USER = ENDPOINT_USERS + "/" + ID_PARAM;
    final static String ENDPOINT_ACCOUNT = ENDPOINT_ACCOUNTS + "/" + ID_PARAM;
    final static String ENDPOINT_DEPOSIT = "/deposit";
    final static String ENDPOINT_TRANSFER = "/transfer";
}
