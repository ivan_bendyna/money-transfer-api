package by.cap.web;

import by.cap.storage.AccountDto;
import by.cap.storage.Storage;
import by.cap.storage.UserDto;
import by.cap.storage.memory.MemoryStorage;
import by.cap.utils.HttpUtils;
import com.google.gson.Gson;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.utils.StringUtils;

import static spark.Spark.*;

/**
 * Server setups endpoints for money transfer API. See API description in README.md.
 */
@SuppressWarnings("unused")
public class Server {

    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    final static String PARAM_NAME = "name";
    final static String PARAM_PHONE = "phone";
    final static String PARAM_CURRENCY = "currency";
    final static String PARAM_USER_ID = "user_id";
    final static String PARAM_ACCOUNT_ID = "account_id";
    final static String PARAM_AMOUNT = "amount";
    final static String PARAM_SOURCE_ACCOUNT_ID = "source_account_id";
    final static String PARAM_DEST_ACCOUNT_ID = "dest_account_id";

    static final String MIME_JSON = "application/json";

    private final static Gson GSON = new Gson();

    private Storage storage;

    /**
     * Create server instance.
     *
     * @param countThreads count threads processing requests
     * @param port         server port or 0 for random available port
     */
    public Server(int port, int countThreads) {
        port(port);
        threadPool(countThreads, countThreads, Integer.MAX_VALUE);
        storage = new MemoryStorage();
    }

    /**
     * Start server.
     */
    public void start() {
        before(this::logRequest, this::filterAcceptHeader);
        path(Endpoints.VERSION, () -> {
            post(Endpoints.ENDPOINT_USERS, this::createUser, GSON::toJson);
            get(Endpoints.ENDPOINT_USER, this::getUser, GSON::toJson);
            delete(Endpoints.ENDPOINT_USER, this::deleteUser, GSON::toJson);

            post(Endpoints.ENDPOINT_ACCOUNTS, this::createAccount, GSON::toJson);
            get(Endpoints.ENDPOINT_ACCOUNT, this::getAccount, GSON::toJson);
            delete(Endpoints.ENDPOINT_ACCOUNT, this::deleteAccount, GSON::toJson);

            post(Endpoints.ENDPOINT_DEPOSIT, this::deposit, GSON::toJson);
            post(Endpoints.ENDPOINT_TRANSFER, this::transfer, GSON::toJson);
        });
    }

    /**
     * Get server port, useful if 0 (random port) was set in parameters.
     *
     * @return server port
     */
    public int getPort() {
        spark.Spark.awaitInitialization();
        return port();
    }

    /**
     * Stop server and await for its stop.
     */
    @SuppressWarnings("WeakerAccess")
    public void stop() {
        spark.Spark.stop();
        spark.Spark.awaitStop();
    }

    private void logRequest(Request request, Response response) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String param : request.queryParams()) {
            stringBuilder.append(param)
                    .append('=')
                    .append(request.queryParams(param))
                    .append('&');
        }
        String params = stringBuilder.length() > 0
                ? stringBuilder.substring(0, stringBuilder.length() - 1)
                : "";
        logger.info("Received API call: {} {} {}", request.requestMethod(), request.url(), params);
    }

    private void filterAcceptHeader(Request request, Response response) {
        String headerValue = request.headers(HttpHeader.ACCEPT.asString());
        if (!StringUtils.isEmpty(headerValue) &&
                !HttpUtils.containsMimeType(headerValue, MIME_JSON)) {
            halt(HttpStatus.NOT_ACCEPTABLE_406, "Supports only application/json");
        }
    }

    private Object createUser(Request request, Response response) {
        String name = haltIfParamIsEmpty(request, PARAM_NAME);
        String phone = haltIfParamIsEmpty(request, PARAM_PHONE);
        UserDto userDto = storage.createUser(name, phone);
        logger.debug("User {} created", userDto.getId());
        response.status(HttpStatus.CREATED_201);
        response.type(MIME_JSON);
        return userDto;
    }

    private Object getUser(Request request, Response response) {
        long id = parseParamId(request);
        UserDto userDto = storage.getUser(id);
        if (userDto == null) {
            logger.debug("User {} not found", id);
            halt(HttpStatus.NOT_FOUND_404);
        }
        response.status(HttpStatus.OK_200);
        response.type(MIME_JSON);
        return userDto;
    }

    private Object deleteUser(Request request, Response response) {
        long id = parseParamId(request);
        boolean deleted = storage.deleteUser(id);
        if (!deleted) {
            logger.debug("User {} not found", id);
            halt(HttpStatus.NOT_FOUND_404);
        }
        logger.debug("User {} deleted", id);
        halt(HttpStatus.OK_200);
        return null;
    }

    private Object createAccount(Request request, Response response) {
        String currency = haltIfParamIsEmpty(request, PARAM_CURRENCY);
        long userId = haltIfParamIsNotLong(request, PARAM_USER_ID);

        AccountDto accountDto = storage.createAccount(userId, currency);
        if (accountDto == null) {
            logger.debug("User {} not found", userId);
            halt(HttpStatus.NOT_FOUND_404, String.format("User with id %d not found", userId));
        }
        logger.debug("Account {} created", accountDto.getId());
        response.status(HttpStatus.CREATED_201);
        response.type(MIME_JSON);
        return accountDto;
    }

    private Object getAccount(Request request, Response response) {
        long id = parseParamId(request);
        AccountDto accountDto = storage.getAccount(id);
        if (accountDto == null) {
            logger.debug("Account {} not found", id);
            halt(HttpStatus.NOT_FOUND_404);
        }
        response.status(HttpStatus.OK_200);
        response.type(MIME_JSON);
        return accountDto;
    }

    private Object deleteAccount(Request request, Response response) {
        long id = parseParamId(request);
        boolean deleted = storage.deleteAccount(id);
        if (!deleted) {
            logger.debug("Account {} not found", id);
            halt(HttpStatus.NOT_FOUND_404);
        }
        logger.debug("Account {} deleted", id);
        halt(HttpStatus.OK_200);
        return null;
    }

    private Object deposit(Request request, Response response) {
        long accountId = haltIfParamIsNotLong(request, PARAM_ACCOUNT_ID);
        long amount = haltIfParamIsNotLong(request, PARAM_AMOUNT);
        if (amount <= 0) {
            logger.debug("Amount {} is not positive", amount);
            halt(HttpStatus.BAD_REQUEST_400, "Amount should be > 0");
        }
        if (!storage.deposit(accountId, amount)) {
            logger.debug("Account {} not found", accountId);
            halt(HttpStatus.NOT_FOUND_404,
                    String.format("Account with id %d not found", accountId));
        }
        logger.debug("Deposit {} for account {} is successful", amount, accountId);
        halt(HttpStatus.OK_200);
        return null;
    }

    private Object transfer(Request request, Response response) {
        long sourceAccountId = haltIfParamIsNotLong(request, PARAM_SOURCE_ACCOUNT_ID);
        long destAccountId = haltIfParamIsNotLong(request, PARAM_DEST_ACCOUNT_ID);
        long amount = haltIfParamIsNotLong(request, PARAM_AMOUNT);
        if (sourceAccountId == destAccountId) {
            logger.debug("Source and destination account are the same");
            halt(HttpStatus.BAD_REQUEST_400,
                    "Accounts are the same, transfer between different accounts");
        }
        if (amount <= 0) {
            logger.debug("Amount {} is not positive", amount);
            halt(HttpStatus.BAD_REQUEST_400, "Amount should be > 0");
        }

        AccountDto sourceAccount = storage.getAccount(sourceAccountId);
        if (sourceAccount == null) {
            logger.debug("Source account {} not found", sourceAccountId);
            halt(HttpStatus.NOT_FOUND_404, "Source account not found");
        }
        AccountDto destAccount = storage.getAccount(destAccountId);
        if (destAccount == null) {
            logger.debug("Destination account {} not found", destAccountId);
            halt(HttpStatus.NOT_FOUND_404, "Destination account not found");
        }
        if (!sourceAccount.getCurrency().equalsIgnoreCase(destAccount.getCurrency())) {
            logger.debug("Accounts have different currency");
            halt(HttpStatus.BAD_REQUEST_400, "Accounts have different currency");
        }

        Storage.TransferResult result = storage.transfer(sourceAccountId, destAccountId, amount);
        if (!result.isSuccessful()) {
            logger.debug("Transfer is not successful, error message: {}", result.getErrorMessage());
            halt(HttpStatus.BAD_REQUEST_400, result.getErrorMessage());
        }
        logger.debug("Transfer {} from {} to {} is successful", amount,
                sourceAccountId, destAccountId);
        halt(HttpStatus.OK_200);
        return null;
    }

    private String haltIfParamIsEmpty(Request request, String paramName) {
        String paramValue = request.queryParams(paramName);
        if (StringUtils.isEmpty(paramValue)) {
            halt(HttpStatus.BAD_REQUEST_400,
                    String.format("Param '%s' is empty", paramName));
        }
        return paramValue;
    }

    private long haltIfParamIsNotLong(Request request, String paramName) {
        return haltIfParamIsNotLong(haltIfParamIsEmpty(request, paramName));
    }

    private long haltIfParamIsNotLong(String paramStr) {
        long longValue = 0;
        try {
            longValue = Long.parseLong(paramStr);
        } catch (NumberFormatException e) {
            halt(HttpStatus.BAD_REQUEST_400,
                    String.format("Wrong value '%s', should be a number", paramStr));
        }
        return longValue;
    }

    private long parseParamId(Request request) {
        String idString = request.params(Endpoints.ID_PARAM);
        return haltIfParamIsNotLong(idString);
    }
}
