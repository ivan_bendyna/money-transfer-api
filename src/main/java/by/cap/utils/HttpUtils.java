package by.cap.utils;

import spark.utils.StringUtils;

public class HttpUtils {

    /**
     * Checks if header contains specific mime type.
     *
     * @param headerValue  header value text
     * @param requiredMime mime, for example application/json
     * @return {@code true} - if header contains mime, {@code false} - otherwise
     */
    public static boolean containsMimeType(String headerValue,
                                           String requiredMime) {
        String[] requiredParts = requiredMime.split("/");
        if (requiredParts.length != 2 || StringUtils.isEmpty(requiredParts[0])
                || StringUtils.isEmpty(requiredParts[1])) {
            throw new IllegalArgumentException(
                    String.format("Mime type %s is invalid", requiredMime));
        }
        for (String mime : headerValue.trim().split(",")) {
            int i = mime.indexOf(";");
            if (i >= 0) {
                mime = mime.substring(0, i);
            }
            String[] parts = mime.trim().split("/");
            if (parts.length != 2) {
                continue;
            }
            if ("*".equals(parts[1]) && ("*".equals(parts[0])
                    || requiredParts[0].equals(parts[0]))) {
                return true;
            }
            if (requiredParts[0].equals(parts[0])
                    && requiredParts[1].equals(parts[1])) {
                return true;
            }
        }
        return false;
    }
}
