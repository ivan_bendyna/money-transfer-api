package by.cap;

import by.cap.web.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        if (args.length != 2) {
            logger.error("There are {} arguments, should be 2", args.length);
            return;
        }
        int port, countThreads;
        try {
            port = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            logger.error("Port is not a number");
            return;
        }
        try {
            countThreads = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            logger.error("Count threads is not a number");
            return;
        }
        Server server = new Server(port, countThreads);
        server.start();
        logger.info("Server started on port {}", server.getPort());
    }
}
