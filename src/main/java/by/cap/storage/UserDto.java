package by.cap.storage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserDto {

    private final long id;
    private final String name;
    private final String phone;
    private final List<AccountDto> accounts;

    public UserDto(long id, String name, String phone, Collection<AccountDto> accounts) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.accounts = new ArrayList<>(accounts);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public List<AccountDto> getAccounts() {
        return accounts;
    }

}
