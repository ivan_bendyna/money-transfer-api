package by.cap.storage;

public class AccountDto {

    private final long id;
    private final String currency;
    private final long balance;

    public AccountDto(long id, String currency, long balance) {
        this.id = id;
        this.balance = balance;
        this.currency = currency;
    }

    public long getId() {
        return id;
    }

    public long getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }
}
