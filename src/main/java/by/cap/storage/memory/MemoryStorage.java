package by.cap.storage.memory;

import by.cap.storage.AccountDto;
import by.cap.storage.Storage;
import by.cap.storage.UserDto;
import by.cap.storage.memory.model.Account;
import by.cap.storage.memory.model.User;
import spark.utils.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;

/**
 * Memory storage implementation. All operations for users and accounts should be
 * locked by user's lock.
 */
public class MemoryStorage implements Storage {

    private static final AtomicLong USER_ID_COUNTER = new AtomicLong();
    private static final AtomicLong ACCOUNT_ID_COUNTER = new AtomicLong();

    private Map<Long, User> users = new ConcurrentHashMap<>();
    private Map<Long, Account> accounts = new ConcurrentHashMap<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDto createUser(String name, String phone) {
        if (StringUtils.isEmpty(name)) {
            throw new IllegalArgumentException("Name should be non-empty string");
        }
        if (StringUtils.isEmpty(phone)) {
            throw new IllegalArgumentException("Phone should be non-empty string");
        }
        long userId = USER_ID_COUNTER.getAndIncrement();
        User user = new User(userId, name, phone);
        UserDto result = user.createDto();
        users.put(userId, user);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserDto getUser(long userId) {
        User user = users.get(userId);
        if (user == null) {
            return null;
        }
        Lock lock = user.getLock();

        lock.lock();
        try {
            user = users.get(userId);
            return user == null ? null : user.createDto();
        } finally {
            lock.unlock();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean deleteUser(long userId) {
        User user = users.get(userId);
        if (user == null) {
            return false;
        }
        Lock lock = user.getLock();

        lock.lock();
        try {
            if (users.remove(userId) != null) {
                user.getAccounts().keySet().forEach(accounts::remove);
                return true;
            } else {
                return false;
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AccountDto createAccount(long userId, String currency) {
        if (StringUtils.isEmpty(currency)) {
            throw new IllegalArgumentException("Currency should be non-empty string");
        }
        User user = users.get(userId);
        if (user == null) {
            return null;
        }
        Lock lock = user.getLock();

        lock.lock();
        try {
            user = users.get(userId);
            if (user == null) {
                return null;
            }
            long accountId = ACCOUNT_ID_COUNTER.getAndIncrement();
            Account account = new Account(accountId, currency, user);
            AccountDto result = account.createDto();
            user.addAccount(account);
            accounts.put(accountId, account);
            return result;
        } finally {
            lock.unlock();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AccountDto getAccount(long accountId) {
        Account account = accounts.get(accountId);
        if (account == null) {
            return null;
        }
        Lock lock = account.getUser().getLock();

        lock.lock();
        try {
            account = accounts.get(accountId);
            return account == null ? null : account.createDto();
        } finally {
            lock.unlock();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean deleteAccount(long accountId) {
        Account account = accounts.get(accountId);
        if (account == null) {
            return false;
        }
        Lock lock = account.getUser().getLock();

        lock.lock();
        try {
            if (accounts.remove(accountId) != null) {
                account.getUser().removeAccount(accountId);
                return true;
            } else {
                return false;
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean deposit(long accountId, long amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Amount should be > 0");
        }

        Account account = accounts.get(accountId);
        if (account == null) {
            return false;
        }
        Lock lock = account.getUser().getLock();

        lock.lock();
        try {
            account = accounts.get(accountId);
            if (account == null) {
                return false;
            }
            account.changeBalance(amount);
            return true;
        } finally {
            lock.unlock();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TransferResult transfer(long sourceAccountId, long destAccountId, long amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Amount should be > 0");
        }
        if (sourceAccountId == destAccountId) {
            throw new IllegalArgumentException("Transfer should be between different accounts");
        }

        Account sourceAccount = accounts.get(sourceAccountId);
        if (sourceAccount == null) {
            return TransferResult.fail("Source account not found");
        }
        Account destAccount = accounts.get(destAccountId);
        if (destAccount == null) {
            return TransferResult.fail("Destination account not found");
        }
        if (!sourceAccount.getCurrency().equals(destAccount.getCurrency())) {
            throw new IllegalStateException(
                    "Transfer can't be made between accounts with different currency");
        }

        // order to avoid deadlocks
        Lock firstLock = sourceAccountId < destAccountId
                ? sourceAccount.getUser().getLock()
                : destAccount.getUser().getLock();
        Lock secondLock = sourceAccountId < destAccountId
                ? destAccount.getUser().getLock()
                : sourceAccount.getUser().getLock();

        firstLock.lock();
        try {
            secondLock.lock();
            try {
                sourceAccount = accounts.get(sourceAccountId);
                if (sourceAccount == null) {
                    return TransferResult.fail("Source account not found");
                }
                destAccount = accounts.get(destAccountId);
                if (destAccount == null) {
                    return TransferResult.fail("Destination account not found");
                }
                if (sourceAccount.getBalance() < amount) {
                    return TransferResult.fail(
                            "Source account does not have enough money for transfer");
                }
                sourceAccount.changeBalance(-amount);
                destAccount.changeBalance(amount);
                return TransferResult.success();
            } finally {
                secondLock.unlock();
            }
        } finally {
            firstLock.unlock();
        }
    }

}
