package by.cap.storage.memory.model;

import by.cap.storage.AccountDto;

/**
 * Account contains id, currency, balance and user which owns this account.
 */
public class Account {

    private final long id;
    private final String currency;
    private final User user;
    private long balance;

    public Account(long id, String currency, User user) {
        this.id = id;
        this.currency = currency;
        this.user = user;
    }

    public void changeBalance(long amount) {
        balance += amount;
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getCurrency() {
        return currency;
    }

    public long getBalance() {
        return balance;
    }

    /**
     * Create {@link AccountDto} object. It's an instant snapshot of Account.
     * Should be called inside of user lock.
     */
    public AccountDto createDto() {
        return new AccountDto(id, currency, balance);
    }
}