package by.cap.storage.memory.model;

import by.cap.storage.UserDto;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * User contains id, name, phone, list of accounts.
 */
public class User {

    private final long id;
    private final String name;
    private final String phone;
    private final Map<Long, Account> accounts = new HashMap<>();
    private final Lock lock = new ReentrantLock();

    public User(long id, String name, String phone) {
        this.id = id;
        this.name = name;
        this.phone = phone;
    }

    public Lock getLock() {
        return lock;
    }

    public Map<Long, Account> getAccounts() {
        return accounts;
    }

    public void addAccount(Account account) {
        accounts.put(account.getId(), account);
    }

    public void removeAccount(long accountId) {
        accounts.remove(accountId);
    }

    /**
     * Create {@link UserDto} object. It's an instant snapshot of User.
     * Should be called inside of lock.
     */
    public UserDto createDto() {
        return new UserDto(id, name, phone,
                accounts.values().stream()
                        .map(Account::createDto)
                        .collect(Collectors.toList()));
    }

}