package by.cap.storage;

public interface Storage {

    /**
     * Create user. Name and phone could be any non-empty strings.
     *
     * @param name  user name
     * @param phone user phone
     * @return created user DTO
     * @throws IllegalArgumentException if name or phone is null or empty
     */
    UserDto createUser(String name, String phone);

    /**
     * Get user by id.
     *
     * @param userId user id
     * @return user DTO or {@code null} if there is no user with such id
     */
    UserDto getUser(long userId);

    /**
     * Delete user.
     *
     * @param userId user id
     * @return {@code true} if user was deleted or {@code false} if there is no user with such id
     */
    boolean deleteUser(long userId);

    /**
     * Create account.
     *
     * @param userId   user id
     * @param currency currency of account
     * @return created account DTO or {@code null} if there is no user with such id
     * @throws IllegalArgumentException if currency is null or empty string
     */
    AccountDto createAccount(long userId, String currency);

    /**
     * Get account by id.
     *
     * @param accountId account id
     * @return account DTO or {@code null} if there is no account with such id
     */
    AccountDto getAccount(long accountId);

    /**
     * Delete account.
     *
     * @param accountId account id
     * @return {@code true} if account was deleted or {@code false}
     * if there is no account with such id
     */
    boolean deleteAccount(long accountId);

    /**
     * Deposit to account.
     *
     * @param accountId account id
     * @param amount    amount of money to deposit
     * @return {@code true} if money deposited or {@code false} if there is no account with such id
     * @throws IllegalArgumentException if {@code amount} <= 0
     */
    boolean deposit(long accountId, long amount);

    /**
     * Transfer money from source account to destination account.
     *
     * @param sourceAccountId source account id
     * @param destAccountId   destination account id
     * @param amount          amount of money to transfer
     * @return {@link TransferResult} object with status and
     * error message (if status is not successful)
     * @throws IllegalArgumentException if {@code amount} <= 0 or source and destination
     *                                  account ids are the same
     * @throws IllegalStateException    if accounts have different currency
     */
    TransferResult transfer(long sourceAccountId, long destAccountId, long amount);

    /**
     * Result of transfer operation. Can be successful without error message or
     * not successful with error message.
     */
    class TransferResult {

        private static final TransferResult SUCCESSFUL = new TransferResult(true, null);

        private final boolean successful;
        private final String errorMessage;

        public static TransferResult success() {
            return SUCCESSFUL;
        }

        public static TransferResult fail(String errorMessage) {
            return new TransferResult(false, errorMessage);
        }

        private TransferResult(boolean successful, String errorMessage) {
            if (successful && errorMessage != null) {
                throw new IllegalArgumentException(
                        "Result is successful, error message should be null");
            }
            if (!successful && errorMessage == null) {
                throw new IllegalArgumentException(
                        "Result is not successful, error message should be not null");
            }
            this.successful = successful;
            this.errorMessage = errorMessage;
        }

        public boolean isSuccessful() {
            return successful;
        }

        public String getErrorMessage() {
            return errorMessage;
        }
    }
}
