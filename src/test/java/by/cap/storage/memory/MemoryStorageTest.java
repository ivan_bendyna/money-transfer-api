package by.cap.storage.memory;

import by.cap.storage.AccountDto;
import by.cap.storage.Storage;
import by.cap.storage.UserDto;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.*;

class MemoryStorageTest {

    private static final String NAME1 = "John";
    private static final String PHONE1 = "+12341112222";
    private static final String NAME2 = "Ivan";
    private static final String PHONE2 = "+12341113333";
    private static final String CURRENCY = "EUR";

    private MemoryStorage memoryStorage;

    @BeforeEach
    void init() throws IllegalAccessException {
        memoryStorage = new MemoryStorage();
        AtomicLong userIdCounter = (AtomicLong) FieldUtils.readStaticField(
                MemoryStorage.class, "USER_ID_COUNTER", true);
        userIdCounter.set(0);
        AtomicLong accountIdCounter = (AtomicLong) FieldUtils.readStaticField(
                MemoryStorage.class, "ACCOUNT_ID_COUNTER", true);
        accountIdCounter.set(0);
    }

    @DisplayName("Test create users and get")
    @Test
    void testCreateFewUsers() {
        UserDto userDto1 = memoryStorage.createUser(NAME1, PHONE1);
        assertEquals(0, userDto1.getId());
        assertEquals(NAME1, userDto1.getName());
        assertEquals(PHONE1, userDto1.getPhone());
        assertTrue(userDto1.getAccounts().isEmpty());

        UserDto userDto2 = memoryStorage.createUser(NAME2, PHONE2);
        assertEquals(1, userDto2.getId());
        assertEquals(NAME2, userDto2.getName());
        assertEquals(PHONE2, userDto2.getPhone());
        assertTrue(userDto2.getAccounts().isEmpty());

        assertNotNull(memoryStorage.getUser(userDto1.getId()));
        assertNotNull(memoryStorage.getUser(userDto2.getId()));
    }

    @DisplayName("Test that users have different ids")
    @Test
    void testUsersDifferentIds() throws InterruptedException {
        int countThreads = 10;
        int countUsers = 1000;
        Set<Long> ids = ConcurrentHashMap.newKeySet();

        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch endLatch = new CountDownLatch(countThreads);
        for (int i = 0; i < countThreads; i++) {
            new Thread(() -> {
                try {
                    startLatch.await();
                } catch (InterruptedException e) {
                    return;
                }
                for (int j = 0; j < countUsers; j++) {
                    long id = memoryStorage.createUser(NAME1, PHONE1).getId();
                    ids.add(id);
                }
                endLatch.countDown();
            }).start();
        }

        startLatch.countDown();
        assertTrue(endLatch.await(10, TimeUnit.SECONDS));

        assertEquals(countThreads * countUsers, ids.size());
    }

    @DisplayName("Test create user, delete and get")
    @Test
    void testCreateFewUsersDelete() {
        UserDto userDto = memoryStorage.createUser(NAME1, PHONE1);
        long userId1 = userDto.getId();
        assertEquals(0, userId1);
        assertEquals(NAME1, userDto.getName());
        assertEquals(PHONE1, userDto.getPhone());
        assertTrue(userDto.getAccounts().isEmpty());

        userDto = memoryStorage.createUser(NAME2, PHONE2);
        long userId2 = userDto.getId();
        assertEquals(1, userId2);
        assertEquals(NAME2, userDto.getName());
        assertEquals(PHONE2, userDto.getPhone());
        assertTrue(userDto.getAccounts().isEmpty());

        assertNotNull(memoryStorage.getUser(userId1));
        assertNotNull(memoryStorage.getUser(userId2));

        assertTrue(memoryStorage.deleteUser(userId1));

        assertNull(memoryStorage.getUser(userId1));
        assertNotNull(memoryStorage.getUser(userId2));

        assertFalse(memoryStorage.deleteUser(userId1));
        assertTrue(memoryStorage.deleteUser(userId2));

        assertNull(memoryStorage.getUser(userId1));
        assertNull(memoryStorage.getUser(userId2));
    }

    @DisplayName("Test create account")
    @Test
    void testCreateAccount() {
        long userId = memoryStorage.createUser(NAME1, PHONE1).getId();
        AccountDto accountDto = memoryStorage.createAccount(userId, CURRENCY);

        assertEquals(0, accountDto.getId());
        assertEquals(CURRENCY, accountDto.getCurrency());
        assertEquals(0, accountDto.getBalance());

        UserDto userDto = memoryStorage.getUser(userId);
        assertEquals(1, userDto.getAccounts().size());

        accountDto = userDto.getAccounts().get(0);
        assertEquals(0, accountDto.getId());
        assertEquals(CURRENCY, accountDto.getCurrency());
        assertEquals(0, accountDto.getBalance());
    }

    @DisplayName("Test create account when user doesn't exit")
    @Test
    void testCreateAccountUserNotExist() {
        long userId = memoryStorage.createUser(NAME1, PHONE1).getId();
        AccountDto accountDto = memoryStorage.createAccount(userId + 1, CURRENCY);

        assertNull(accountDto);
    }

    @DisplayName("Test that accounts have different ids")
    @Test
    void testAccountsDifferentIds() throws InterruptedException {
        long userId = memoryStorage.createUser(NAME1, PHONE1).getId();
        int countThreads = 10;
        int countAccounts = 1000;
        Set<Long> ids = ConcurrentHashMap.newKeySet();

        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch endLatch = new CountDownLatch(countThreads);
        for (int i = 0; i < countThreads; i++) {
            new Thread(() -> {
                try {
                    startLatch.await();
                } catch (InterruptedException e) {
                    return;
                }
                for (int j = 0; j < countAccounts; j++) {
                    long id = memoryStorage.createAccount(userId, CURRENCY).getId();
                    ids.add(id);
                }
                endLatch.countDown();
            }).start();
        }

        startLatch.countDown();
        assertTrue(endLatch.await(10, TimeUnit.SECONDS));

        assertEquals(countThreads * countAccounts, ids.size());
    }

    @DisplayName("Test delete user with accounts")
    @Test
    void testDeleteUserWithAccount() {
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        assertEquals(0, userId1);
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        assertEquals(0, accountId1);

        long userId2 = memoryStorage.createUser(NAME2, PHONE2).getId();
        assertEquals(1, userId2);
        long accountId2 = memoryStorage.createAccount(userId2, CURRENCY).getId();
        assertEquals(1, accountId2);

        assertNotNull(memoryStorage.getAccount(accountId1));
        assertNotNull(memoryStorage.getAccount(accountId2));

        assertTrue(memoryStorage.deleteUser(userId1));

        assertNull(memoryStorage.getAccount(accountId1));
        assertNotNull(memoryStorage.getAccount(accountId2));
    }

    @DisplayName("Test delete account of user")
    @Test
    void testDeleteAccount() {
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        assertEquals(0, userId1);
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        assertEquals(0, accountId1);
        long accountId2 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        assertEquals(1, accountId2);

        assertNotNull(memoryStorage.getAccount(accountId1));
        assertNotNull(memoryStorage.getAccount(accountId2));
        assertEquals(2, memoryStorage.getUser(userId1).getAccounts().size());

        assertTrue(memoryStorage.deleteAccount(accountId1));

        assertNull(memoryStorage.getAccount(accountId1));
        assertNotNull(memoryStorage.getAccount(accountId2));
        UserDto userDto = memoryStorage.getUser(userId1);
        assertEquals(1, userDto.getAccounts().size());
        assertEquals(accountId2, userDto.getAccounts().get(0).getId());
    }

    @DisplayName("Delete user should return true only once")
    @Test
    void testDeleteUserConcurrently() throws InterruptedException {
        long userId = memoryStorage.createUser(NAME1, PHONE1).getId();

        int countThreads = 8;
        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch endLatch = new CountDownLatch(countThreads);
        AtomicInteger countDeleted = new AtomicInteger();
        for (int i = 0; i < countThreads; i++) {
            new Thread(() -> {
                try {
                    startLatch.await();
                } catch (InterruptedException e) {
                    return;
                }
                if (memoryStorage.deleteUser(userId)) {
                    countDeleted.incrementAndGet();
                }
                endLatch.countDown();
            }).start();
        }

        startLatch.countDown();
        assertTrue(endLatch.await(10, TimeUnit.SECONDS));
        assertEquals(1, countDeleted.get());
    }

    @DisplayName("Delete account should return true only once")
    @Test
    void testDeleteAccountConcurrent() throws InterruptedException {
        long userId = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId = memoryStorage.createAccount(userId, CURRENCY).getId();

        int countThreads = 8;
        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch endLatch = new CountDownLatch(countThreads);
        AtomicInteger countDeleted = new AtomicInteger();
        for (int i = 0; i < countThreads; i++) {
            new Thread(() -> {
                try {
                    startLatch.await();
                } catch (InterruptedException e) {
                    return;
                }
                if (memoryStorage.deleteAccount(accountId)) {
                    countDeleted.incrementAndGet();
                }
                endLatch.countDown();
            }).start();
        }

        startLatch.countDown();
        assertTrue(endLatch.await(10, TimeUnit.SECONDS));
        assertEquals(1, countDeleted.get());
    }

    @DisplayName("Test deposit")
    @Test
    void testDeposit() {
        UserDto userDto = memoryStorage.createUser(NAME1, PHONE1);
        AccountDto accountDto1 = memoryStorage.createAccount(userDto.getId(), CURRENCY);
        assertEquals(0, accountDto1.getBalance());
        AccountDto accountDto2 = memoryStorage.createAccount(userDto.getId(), CURRENCY);
        assertEquals(0, accountDto2.getBalance());

        assertTrue(memoryStorage.deposit(accountDto1.getId(), 10));
        assertTrue(memoryStorage.deposit(accountDto2.getId(), 15));

        assertEquals(10, memoryStorage.getAccount(accountDto1.getId()).getBalance());
        assertEquals(15, memoryStorage.getAccount(accountDto2.getId()).getBalance());
    }

    @DisplayName("Test deposit negative amount")
    @Test
    void testDepositNegative() {
        long userId = memoryStorage.createUser(NAME1, PHONE1).getId();
        AccountDto accountDto1 = memoryStorage.createAccount(userId, CURRENCY);
        assertEquals(0, accountDto1.getBalance());

        assertThrows(IllegalArgumentException.class,
                () -> memoryStorage.deposit(accountDto1.getId(), -1));

        assertEquals(0, memoryStorage.getAccount(accountDto1.getId()).getBalance());
    }

    @DisplayName("Test deposit when account doesn't exist")
    @Test
    void testDepositAccountNotExist() {
        assertFalse(memoryStorage.deposit(0, 1));
    }

    @DisplayName("Test deposits concurrently")
    @Test
    void testDepositsConcurrently() throws InterruptedException {
        long userId = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId = memoryStorage.createAccount(userId, CURRENCY).getId();

        int countThreads = 8;
        int countDepositsPerThread = 1000;
        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch endLatch = new CountDownLatch(countThreads);
        for (int i = 0; i < countThreads; i++) {
            new Thread(() -> {
                try {
                    startLatch.await();
                } catch (InterruptedException e) {
                    return;
                }
                for (int j = 0; j < countDepositsPerThread; j++) {
                    memoryStorage.deposit(accountId, 1);
                }
                endLatch.countDown();
            }).start();
        }

        startLatch.countDown();
        assertTrue(endLatch.await(10, TimeUnit.SECONDS));

        long balance = memoryStorage.getAccount(accountId).getBalance();
        assertEquals(countDepositsPerThread * countThreads, balance);
    }

    @DisplayName("Test transfer")
    @Test
    void testTransfer() {
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();

        long userId2 = memoryStorage.createUser(NAME2, PHONE2).getId();
        long accountId2 = memoryStorage.createAccount(userId2, CURRENCY).getId();

        memoryStorage.deposit(accountId1, 10);
        assertEquals(10, memoryStorage.getAccount(accountId1).getBalance());

        Storage.TransferResult result = memoryStorage.transfer(accountId1, accountId2, 3);
        assertTrue(result.isSuccessful());
        assertEquals(7, memoryStorage.getAccount(accountId1).getBalance());
        assertEquals(3, memoryStorage.getAccount(accountId2).getBalance());
    }

    @DisplayName("Test transfer more money than balance")
    @Test
    void testTransferMoreBalance() {
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();

        long userId2 = memoryStorage.createUser(NAME2, PHONE2).getId();
        long accountId2 = memoryStorage.createAccount(userId2, CURRENCY).getId();

        memoryStorage.deposit(accountId1, 10);
        assertEquals(10, memoryStorage.getAccount(accountId1).getBalance());

        Storage.TransferResult result = memoryStorage.transfer(accountId1, accountId2, 11);
        assertFalse(result.isSuccessful());
        assertEquals("Source account does not have enough money for transfer",
                result.getErrorMessage());
        assertEquals(10, memoryStorage.getAccount(accountId1).getBalance());
        assertEquals(0, memoryStorage.getAccount(accountId2).getBalance());
    }

    @DisplayName("Transfer negative amount")
    @Test
    void testTransferNegativeAmount() {
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        long accountId2 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        memoryStorage.deposit(accountId1, 10);

        assertThrows(IllegalArgumentException.class,
                () -> memoryStorage.transfer(accountId1, accountId2, -1));
        assertEquals(10, memoryStorage.getAccount(accountId1).getBalance());
        assertEquals(0, memoryStorage.getAccount(accountId2).getBalance());
    }

    @DisplayName("Transfer same account")
    @Test
    void testTransferSameAccount() {
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        memoryStorage.deposit(accountId1, 10);

        assertThrows(IllegalArgumentException.class,
                () -> memoryStorage.transfer(accountId1, accountId1, 1));
        assertEquals(10, memoryStorage.getAccount(accountId1).getBalance());
    }

    @DisplayName("Transfer different currency")
    @Test
    void testTransferDifferentCurrency() {
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        long accountId2 = memoryStorage.createAccount(userId1, "BYN").getId();
        memoryStorage.deposit(accountId1, 10);

        assertThrows(IllegalStateException.class,
                () -> memoryStorage.transfer(accountId1, accountId2, 1));
        assertEquals(10, memoryStorage.getAccount(accountId1).getBalance());
        assertEquals(0, memoryStorage.getAccount(accountId2).getBalance());
    }

    @DisplayName("Transfer from non-existent source account")
    @Test
    void testTransferNonExistentSourceAccount() {
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();

        Storage.TransferResult result = memoryStorage.transfer(5, accountId1, 1);
        assertFalse(result.isSuccessful());
        assertEquals("Source account not found", result.getErrorMessage());
        assertEquals(0, memoryStorage.getAccount(accountId1).getBalance());
    }

    @DisplayName("Transfer on non-existent destination account")
    @Test
    void testTransferNonExistentDestinationAccount() {
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        memoryStorage.deposit(accountId1, 10);

        Storage.TransferResult result = memoryStorage.transfer(accountId1, 5, 1);
        assertFalse(result.isSuccessful());
        assertEquals("Destination account not found", result.getErrorMessage());
        assertEquals(10, memoryStorage.getAccount(accountId1).getBalance());
    }

    @DisplayName("Transfers between different users concurrently")
    @Test
    void testTransferConcurrently() throws InterruptedException {
        int count = 10000;
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        memoryStorage.deposit(accountId1, count);

        long userId2 = memoryStorage.createUser(NAME2, PHONE2).getId();
        long accountId2 = memoryStorage.createAccount(userId2, CURRENCY).getId();
        memoryStorage.deposit(accountId2, count);

        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch endLatch = new CountDownLatch(2);
        new Thread(() -> {
            try {
                startLatch.await();
            } catch (InterruptedException e) {
                return;
            }
            for (int i = 0; i < count; i++) {
                memoryStorage.transfer(accountId1, accountId2, 1);
            }
            endLatch.countDown();
        }).start();
        new Thread(() -> {
            try {
                startLatch.await();
            } catch (InterruptedException e) {
                return;
            }
            for (int i = 0; i < count; i++) {
                memoryStorage.transfer(accountId2, accountId1, 1);
            }
            endLatch.countDown();
        }).start();

        startLatch.countDown();
        assertTrue(endLatch.await(10, TimeUnit.SECONDS), "Timeout, possibly deadlock");

        assertEquals(count, memoryStorage.getAccount(accountId1).getBalance());
        assertEquals(count, memoryStorage.getAccount(accountId2).getBalance());
    }

    @DisplayName("Test that state is consistent while transfer concurrently")
    @Test
    void testTransferConcurrentlyStateConsistent() throws InterruptedException {
        int count = 10000;
        long userId1 = memoryStorage.createUser(NAME1, PHONE1).getId();
        long accountId1 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        long accountId2 = memoryStorage.createAccount(userId1, CURRENCY).getId();
        memoryStorage.deposit(accountId1, count);
        memoryStorage.deposit(accountId2, count);

        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch endLatch = new CountDownLatch(2);
        new Thread(() -> {
            try {
                startLatch.await();
            } catch (InterruptedException e) {
                return;
            }
            for (int i = 0; i < count; i++) {
                memoryStorage.transfer(accountId1, accountId2, 1);
            }
            endLatch.countDown();
        }).start();
        new Thread(() -> {
            try {
                startLatch.await();
            } catch (InterruptedException e) {
                return;
            }
            for (int i = 0; i < count; i++) {
                memoryStorage.transfer(accountId2, accountId1, 1);
            }
            endLatch.countDown();
        }).start();
        Thread stateThread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                UserDto userDto = memoryStorage.getUser(userId1);
                long sum = userDto.getAccounts().stream().mapToLong(AccountDto::getBalance).sum();
                assertEquals(count * 2, sum);
            }
        });
        stateThread.start();

        startLatch.countDown();
        assertTrue(endLatch.await(10, TimeUnit.SECONDS), "Timeout, possibly deadlock");
        assertTrue(stateThread.isAlive());
        stateThread.interrupt();
        stateThread.join();

        assertEquals(count, memoryStorage.getAccount(accountId1).getBalance());
        assertEquals(count, memoryStorage.getAccount(accountId2).getBalance());

    }

}