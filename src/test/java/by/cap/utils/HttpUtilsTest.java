package by.cap.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HttpUtilsTest {

    @DisplayName("Single mime")
    @Test
    void testContainSingle() {
        String headerValue = "application/json";
        assertTrue(HttpUtils.containsMimeType(headerValue, "application/json"));
        assertFalse(HttpUtils.containsMimeType(headerValue, "application/xml"));
    }

    @DisplayName("Single mime with quality")
    @Test
    void testContainSingleWithQuality() {
        String headerValue = "application/json;q=0.3";
        assertTrue(HttpUtils.containsMimeType(headerValue, "application/json"));
        assertFalse(HttpUtils.containsMimeType(headerValue, "application/xml"));
    }

    @DisplayName("Single mime with spaces")
    @Test
    void testContainSingleWithSpaces() {
        String headerValue = " application/json  ";
        assertTrue(HttpUtils.containsMimeType(headerValue, "application/json"));
        assertFalse(HttpUtils.containsMimeType(headerValue, "application/xml"));
    }

    @DisplayName("Multiple mimes")
    @Test
    void testContainMultiple() {
        String headerValue = " application/json;q=0.1  application/text;q=0.3  ";
        assertTrue(HttpUtils.containsMimeType(headerValue, "application/json"));
        assertFalse(HttpUtils.containsMimeType(headerValue, "application/xml"));
    }

    @DisplayName("Any type")
    @Test
    void testContainAnyType() {
        String headerValue = " */*;q=0.1 ";
        assertTrue(HttpUtils.containsMimeType(headerValue, "application/json"));
        assertTrue(HttpUtils.containsMimeType(headerValue, "application/xml"));
        assertTrue(HttpUtils.containsMimeType(headerValue, "text/html"));
    }

    @DisplayName("Any subtype")
    @Test
    void testContainAnySubtype() {
        String headerValue = " application/*;q=0.1 ";
        assertTrue(HttpUtils.containsMimeType(headerValue, "application/json"));
        assertTrue(HttpUtils.containsMimeType(headerValue, "application/xml"));
        assertFalse(HttpUtils.containsMimeType(headerValue, "text/html"));
    }

}