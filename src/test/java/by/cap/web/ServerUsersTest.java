package by.cap.web;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static by.cap.web.ServerTestUtils.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

class ServerUsersTest {

    private static final String NAME1 = "John B.";
    private static final String PHONE1 = "+12341112222";
    private static final String NAME2 = "Ivan Mt&P";
    private static final String PHONE2 = "+12341113333";

    private Server server;

    @BeforeEach
    void before() throws IllegalAccessException {
        server = new Server(0, 8);
        beforeEachTest(server);
    }

    @AfterEach
    void after() {
        afterEachTest(server);
    }

    @DisplayName("Create new user: happy path")
    @Test
    void testNewUser() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
    }

    @DisplayName("Create new user: no param 'name'")
    @Test
    void testNewUserNoName() {
        given()
                .param(Server.PARAM_PHONE, PHONE1)

                .when()
                .post(Endpoints.ENDPOINT_USERS)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'name' is empty"));
    }

    @DisplayName("Create new user: empty param 'name'")
    @Test
    void testNewUserEmptyName() {
        given()
                .param(Server.PARAM_NAME, "")
                .param(Server.PARAM_PHONE, PHONE1)

                .when()
                .post(Endpoints.ENDPOINT_USERS)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'name' is empty"));
    }

    @DisplayName("Create new user: no param 'phone'")
    @Test
    void testNewUserNoPhone() {
        given()
                .param(Server.PARAM_NAME, NAME1)

                .when()
                .post(Endpoints.ENDPOINT_USERS)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'phone' is empty"));
    }

    @DisplayName("Create new user: empty param 'phone'")
    @Test
    void testNewUserEmptyPhone() {
        given()
                .param(Server.PARAM_NAME, NAME1)
                .param(Server.PARAM_PHONE, "")

                .when()
                .post(Endpoints.ENDPOINT_USERS)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'phone' is empty"));
    }

    @DisplayName("Create new user: few users")
    @Test
    void testFewNewUsers() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateUser(NAME2, PHONE2, 1);
    }

    @DisplayName("Create new user: accept xml")
    @Test
    void testNewUserAcceptXml() {
        given()
                .param(Server.PARAM_NAME, NAME1)
                .param(Server.PARAM_PHONE, PHONE1)
                .accept("application/xml")

                .when()
                .post(Endpoints.ENDPOINT_USERS)

                .then()
                .statusCode(HttpStatus.NOT_ACCEPTABLE_406);
    }

    @DisplayName("Test get user")
    @Test
    void testGetUser() {
        checkGetUser404(0);
        checkGetUser404(1);

        checkSuccessfulCreateUser(NAME1, PHONE1, 0);

        checkGetUser(0, NAME1, PHONE1);
        checkGetUser404(1);

        checkSuccessfulCreateUser(NAME2, PHONE2, 1);

        checkGetUser(0, NAME1, PHONE1);
        checkGetUser(1, NAME2, PHONE2);
    }

    @DisplayName("Test get user: non-number parameter")
    @Test
    void testGetUserNonNumber() {
        given()
                .when()
                .get(Endpoints.ENDPOINT_USER.replace(Endpoints.ID_PARAM, "text"))

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 'text', should be a number"));
    }

    @DisplayName("Test delete user")
    @Test
    void testDeleteUser() {
        checkDeleteUser404(0);
        checkDeleteUser404(1);

        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateUser(NAME2, PHONE2, 1);

        checkGetUser(0, NAME1, PHONE1);
        checkGetUser(1, NAME2, PHONE2);

        checkDeleteUser(0);

        checkGetUser404(0);
        checkGetUser(1, NAME2, PHONE2);

        checkDeleteUser404(0);
        checkDeleteUser(1);

        checkGetUser404(0);
        checkGetUser404(1);
    }

    @DisplayName("Test delete user: non-number parameter")
    @Test
    void testDeleteUserNonNumber() {
        given()
                .when()
                .delete(Endpoints.ENDPOINT_USER.replace(Endpoints.ID_PARAM, "text"))

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 'text', should be a number"));
    }

}