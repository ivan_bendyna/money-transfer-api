package by.cap.web;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static by.cap.web.ServerTestUtils.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

class ServerDepositTest {

    private static final String NAME1 = "John B.";
    private static final String PHONE1 = "+12341112222";
    private static final String CURRENCY = "EUR";

    private Server server;

    @BeforeEach
    void before() throws IllegalAccessException {
        server = new Server(0, 8);
        beforeEachTest(server);
    }

    @AfterEach
    void after() {
        afterEachTest(server);
    }

    @DisplayName("Test deposit: happy path")
    @Test
    void testDeposit() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY, 0);

        checkDeposit(0, 10);

        checkGetAccount(0, CURRENCY, 10);
        checkGetUser(0, NAME1, PHONE1, Collections.singletonList(
                Arrays.asList("0", CURRENCY, "10")
        ));
    }

    @DisplayName("Test deposit: no account id")
    @Test
    void testDepositNoAccountId() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY, 0);

        given()
                .param(Server.PARAM_AMOUNT, 1)

                .when()
                .post(Endpoints.ENDPOINT_DEPOSIT)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'account_id' is empty"));
    }

    @DisplayName("Test deposit: no amount")
    @Test
    void testDepositNoAmount() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY, 0);

        given()
                .param(Server.PARAM_ACCOUNT_ID, 0)

                .when()
                .post(Endpoints.ENDPOINT_DEPOSIT)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'amount' is empty"));
    }

    @DisplayName("Test deposit: account id is not a number")
    @Test
    void testDepositAccountIdNotNumber() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY, 0);

        given()
                .param(Server.PARAM_ACCOUNT_ID, "t")
                .param(Server.PARAM_AMOUNT, 1)

                .when()
                .post(Endpoints.ENDPOINT_DEPOSIT)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 't', should be a number"));
    }

    @DisplayName("Test deposit: amount is not a number")
    @Test
    void testDepositAmountNotNumber() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY, 0);

        given()
                .param(Server.PARAM_ACCOUNT_ID, 0)
                .param(Server.PARAM_AMOUNT, "t")

                .when()
                .post(Endpoints.ENDPOINT_DEPOSIT)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 't', should be a number"));
    }

    @DisplayName("Test deposit: amount is negative")
    @Test
    void testDepositAmountNegative() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY, 0);

        given()
                .param(Server.PARAM_ACCOUNT_ID, 0)
                .param(Server.PARAM_AMOUNT, -2)

                .when()
                .post(Endpoints.ENDPOINT_DEPOSIT)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Amount should be > 0"));
    }

    @DisplayName("Test deposit: account not found")
    @Test
    void testDepositAccountNotFound() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY, 0);

        given()
                .param(Server.PARAM_ACCOUNT_ID, 1)
                .param(Server.PARAM_AMOUNT, 2)

                .when()
                .post(Endpoints.ENDPOINT_DEPOSIT)

                .then()
                .statusCode(HttpStatus.NOT_FOUND_404)
                .body(equalTo("Account with id 1 not found"));
    }

}
