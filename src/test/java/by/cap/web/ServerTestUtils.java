package by.cap.web;

import by.cap.storage.memory.MemoryStorage;
import io.restassured.RestAssured;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.eclipse.jetty.http.HttpStatus;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ServerTestUtils {

    static void beforeEachTest(Server server) throws IllegalAccessException {
        server.start();
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = server.getPort();
        RestAssured.basePath = Endpoints.VERSION;

        AtomicLong userIdCounter = (AtomicLong) FieldUtils.readStaticField(
                MemoryStorage.class, "USER_ID_COUNTER", true);
        userIdCounter.set(0);
        AtomicLong accountIdCounter = (AtomicLong) FieldUtils.readStaticField(
                MemoryStorage.class, "ACCOUNT_ID_COUNTER", true);
        accountIdCounter.set(0);
    }

    static void afterEachTest(Server server) {
        server.stop();
    }

    static void checkSuccessfulCreateUser(String name, String phone, int expectedId) {
        given()
                .param(Server.PARAM_NAME, name)
                .param(Server.PARAM_PHONE, phone)

                .when()
                .post(Endpoints.ENDPOINT_USERS)

                .then()
                .statusCode(HttpStatus.CREATED_201)
                .contentType(Server.MIME_JSON)
                .body("id", equalTo(expectedId))
                .body("name", equalTo(name))
                .body("phone", equalTo(phone));
    }

    static void checkGetUser404(long userId) {
        given()
                .when()
                .get(Endpoints.ENDPOINT_USER.replace(Endpoints.ID_PARAM, Long.toString(userId)))

                .then()
                .statusCode(HttpStatus.NOT_FOUND_404)
                .body(equalTo(""));
    }

    static void checkGetUser(int userId, String name, String phone) {
        checkGetUser(userId, name, phone, Collections.emptyList());
    }

    static void checkGetUser(int userId, String name, String phone,
                             List<List<String>> expectedAccounts) {
        List<Map<String, Object>> accounts = given()
                .when()
                .get(Endpoints.ENDPOINT_USER.replace(Endpoints.ID_PARAM, Long.toString(userId)))

                .then()
                .statusCode(HttpStatus.OK_200)
                .contentType(Server.MIME_JSON)
                .body("id", equalTo(userId))
                .body("name", equalTo(name))
                .body("phone", equalTo(phone))

                .extract()
                .path("accounts");

        assertEquals(accounts.size(), expectedAccounts.size());

        Map<String, Map<String, Object>> accountsMap = accounts.stream().collect(Collectors.toMap(
                map -> map.get("id").toString(),
                Function.identity()
        ));

        for (List<String> expectedAccount : expectedAccounts) {
            Map<String, Object> account = accountsMap.get(expectedAccount.get(0));
            assertEquals(expectedAccount.get(0), account.get("id").toString());
            assertEquals(expectedAccount.get(1), account.get("currency").toString());
            assertEquals(expectedAccount.get(2), account.get("balance").toString());
        }
    }

    static void checkDeleteUser404(long userId) {
        given()
                .when()
                .delete(Endpoints.ENDPOINT_USER.replace(Endpoints.ID_PARAM, Long.toString(userId)))

                .then()
                .statusCode(HttpStatus.NOT_FOUND_404)
                .body(equalTo(""));
    }

    static void checkDeleteUser(int userId) {
        given()
                .when()
                .delete(Endpoints.ENDPOINT_USER.replace(Endpoints.ID_PARAM, Long.toString(userId)))

                .then()
                .statusCode(HttpStatus.OK_200)
                .body(equalTo(""));
    }

    static void checkSuccessfulCreateAccount(int userId, String currency, int expectedId) {
        given()
                .param(Server.PARAM_USER_ID, userId)
                .param(Server.PARAM_CURRENCY, currency)

                .when()
                .post(Endpoints.ENDPOINT_ACCOUNTS)

                .then()
                .statusCode(HttpStatus.CREATED_201)
                .contentType(Server.MIME_JSON)
                .body("id", equalTo(expectedId))
                .body("balance", equalTo(0));
    }

    static void checkGetAccount404(long accountId) {
        given()
                .when()
                .get(Endpoints.ENDPOINT_ACCOUNT.replace(Endpoints.ID_PARAM,
                        Long.toString(accountId)))

                .then()
                .statusCode(HttpStatus.NOT_FOUND_404)
                .body(equalTo(""));
    }

    static void checkGetAccount(int accountId, String expectedCurrency, int expectedBalance) {
        given()
                .when()
                .get(Endpoints.ENDPOINT_ACCOUNT.replace(Endpoints.ID_PARAM,
                        Long.toString(accountId)))

                .then()
                .statusCode(HttpStatus.OK_200)
                .contentType(Server.MIME_JSON)
                .body("id", equalTo(accountId))
                .body("currency", equalTo(expectedCurrency))
                .body("balance", equalTo(expectedBalance));
    }

    static void checkDeleteAccount404(long accountId) {
        given()
                .when()
                .delete(Endpoints.ENDPOINT_ACCOUNT.replace(Endpoints.ID_PARAM,
                        Long.toString(accountId)))

                .then()
                .statusCode(HttpStatus.NOT_FOUND_404)
                .body(equalTo(""));
    }

    static void checkDeleteAccount(int accountId) {
        given()
                .when()
                .delete(Endpoints.ENDPOINT_ACCOUNT.replace(Endpoints.ID_PARAM,
                        Long.toString(accountId)))

                .then()
                .statusCode(HttpStatus.OK_200)
                .body(equalTo(""));
    }

    static void checkDeposit(int accountId, int amount) {
        given()
                .param(Server.PARAM_ACCOUNT_ID, accountId)
                .param(Server.PARAM_AMOUNT, amount)

                .when()
                .post(Endpoints.ENDPOINT_DEPOSIT)

                .then()
                .statusCode(HttpStatus.OK_200)
                .body(equalTo(""));
    }

}
