package by.cap.web;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static by.cap.web.ServerTestUtils.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

class ServerTransferTest {

    private static final String NAME1 = "John B.";
    private static final String PHONE1 = "+12341112222";
    private static final String NAME2 = "Ivan Mt&P";
    private static final String PHONE2 = "+12341113333";
    private static final String CURRENCY1 = "EUR";
    private static final String CURRENCY2 = "BYN";

    private Server server;

    @BeforeEach
    void before() throws IllegalAccessException {
        server = new Server(0, 8);
        beforeEachTest(server);
    }

    @AfterEach
    void after() {
        afterEachTest(server);
    }

    @DisplayName("Test transfer: happy path")
    @Test
    void testTransfer() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY1, 0);
        checkSuccessfulCreateUser(NAME2, PHONE2, 1);
        checkSuccessfulCreateAccount(1, CURRENCY1, 1);
        checkDeposit(0, 100);
        checkDeposit(1, 23);

        checkGetAccount(0, CURRENCY1, 100);
        checkGetUser(0, NAME1, PHONE1, Collections.singletonList(
                Arrays.asList("0", CURRENCY1, "100")
        ));
        checkGetAccount(1, CURRENCY1, 23);
        checkGetUser(1, NAME2, PHONE2, Collections.singletonList(
                Arrays.asList("1", CURRENCY1, "23")
        ));

        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 1)
                .param(Server.PARAM_AMOUNT, 20)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.OK_200)
                .body(equalTo(""));

        checkGetAccount(0, CURRENCY1, 80);
        checkGetUser(0, NAME1, PHONE1, Collections.singletonList(
                Arrays.asList("0", CURRENCY1, "80")
        ));
        checkGetAccount(1, CURRENCY1, 43);
        checkGetUser(1, NAME2, PHONE2, Collections.singletonList(
                Arrays.asList("1", CURRENCY1, "43")
        ));

        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 1)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 0)
                .param(Server.PARAM_AMOUNT, 15)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.OK_200)
                .body(equalTo(""));

        checkGetAccount(0, CURRENCY1, 95);
        checkGetUser(0, NAME1, PHONE1, Collections.singletonList(
                Arrays.asList("0", CURRENCY1, "95")
        ));
        checkGetAccount(1, CURRENCY1, 28);
        checkGetUser(1, NAME2, PHONE2, Collections.singletonList(
                Arrays.asList("1", CURRENCY1, "28")
        ));
    }

    @DisplayName("Test transfer: no source account id")
    @Test
    void testTransferNoSourceAccountId() {
        given()
                .param(Server.PARAM_DEST_ACCOUNT_ID, 0)
                .param(Server.PARAM_AMOUNT, 15)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'source_account_id' is empty"));
    }

    @DisplayName("Test transfer: no destination account id")
    @Test
    void testTransferNoDestAccountId() {
        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_AMOUNT, 15)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'dest_account_id' is empty"));
    }

    @DisplayName("Test transfer: no amount")
    @Test
    void testTransferNoAmount() {
        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 1)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'amount' is empty"));
    }

    @DisplayName("Test transfer: source account id is not a number")
    @Test
    void testTransferSourceAccountIdNotNumber() {
        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, "t")
                .param(Server.PARAM_DEST_ACCOUNT_ID, 0)
                .param(Server.PARAM_AMOUNT, 15)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 't', should be a number"));
    }

    @DisplayName("Test transfer: destination account id is not a number")
    @Test
    void testTransferDestAccountIdNotNumber() {
        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_DEST_ACCOUNT_ID, "t")
                .param(Server.PARAM_AMOUNT, 15)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 't', should be a number"));
    }

    @DisplayName("Test transfer: amount is not a number")
    @Test
    void testTransferAmountNotNumber() {
        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 1)
                .param(Server.PARAM_AMOUNT, "t")

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 't', should be a number"));
    }

    @DisplayName("Test transfer: amount is negative")
    @Test
    void testTransferAmountNegative() {
        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 1)
                .param(Server.PARAM_AMOUNT, -5)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Amount should be > 0"));
    }

    @DisplayName("Test transfer: same account")
    @Test
    void testTransferSameAccount() {
        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 0)
                .param(Server.PARAM_AMOUNT, 5)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Accounts are the same, transfer between different accounts"));
    }

    @DisplayName("Test transfer: source account not found")
    @Test
    void testTransferSourceAccountNotFound() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY1, 0);

        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 1)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 0)
                .param(Server.PARAM_AMOUNT, 5)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.NOT_FOUND_404)
                .body(equalTo("Source account not found"));
    }

    @DisplayName("Test transfer: destination account not found")
    @Test
    void testTransferDestinationAccountNotFound() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY1, 0);

        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 1)
                .param(Server.PARAM_AMOUNT, 5)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.NOT_FOUND_404)
                .body(equalTo("Destination account not found"));
    }

    @DisplayName("Test transfer: different currencies")
    @Test
    void testTransferDifferentCurrencies() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY1, 0);
        checkSuccessfulCreateUser(NAME1, PHONE1, 1);
        checkSuccessfulCreateAccount(1, CURRENCY2, 1);

        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 1)
                .param(Server.PARAM_AMOUNT, 5)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Accounts have different currency"));
    }

    @DisplayName("Test transfer: not enough money")
    @Test
    void testTransferNotEnoughMoney() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY1, 0);
        checkSuccessfulCreateUser(NAME1, PHONE1, 1);
        checkSuccessfulCreateAccount(1, CURRENCY1, 1);
        checkDeposit(0, 100);

        given()
                .param(Server.PARAM_SOURCE_ACCOUNT_ID, 0)
                .param(Server.PARAM_DEST_ACCOUNT_ID, 1)
                .param(Server.PARAM_AMOUNT, 101)

                .when()
                .post(Endpoints.ENDPOINT_TRANSFER)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400);
    }

}
