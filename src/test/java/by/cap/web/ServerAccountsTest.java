package by.cap.web;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static by.cap.web.ServerTestUtils.*;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

class ServerAccountsTest {

    private static final String NAME1 = "John B.";
    private static final String PHONE1 = "+12341112222";
    private static final String CURRENCY1 = "EUR";
    private static final String CURRENCY2 = "BYN";

    private Server server;

    @BeforeEach
    void before() throws IllegalAccessException {
        server = new Server(0, 8);
        beforeEachTest(server);
    }

    @AfterEach
    void after() {
        afterEachTest(server);
    }

    @DisplayName("Create new account: happy path")
    @Test
    void testNewAccount() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);

        checkSuccessfulCreateAccount(0, CURRENCY1, 0);
    }

    @DisplayName("Create new account: no currency")
    @Test
    void testNewAccountNoCurrency() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);

        given()
                .param(Server.PARAM_USER_ID, 0)

                .when()
                .post(Endpoints.ENDPOINT_ACCOUNTS)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'currency' is empty"));
    }

    @DisplayName("Create new account: empty currency")
    @Test
    void testNewAccountEmptyCurrency() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);

        given()
                .param(Server.PARAM_USER_ID, 0)
                .param(Server.PARAM_CURRENCY, "")

                .when()
                .post(Endpoints.ENDPOINT_ACCOUNTS)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'currency' is empty"));
    }

    @DisplayName("Create new account: no user id")
    @Test
    void testNewAccountNoUserId() {
        given()
                .param(Server.PARAM_CURRENCY, CURRENCY1)

                .when()
                .post(Endpoints.ENDPOINT_ACCOUNTS)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'user_id' is empty"));
    }

    @DisplayName("Create new account: empty user id")
    @Test
    void testNewAccountEmptyUserId() {
        given()
                .param(Server.PARAM_USER_ID, "")
                .param(Server.PARAM_CURRENCY, CURRENCY1)

                .when()
                .post(Endpoints.ENDPOINT_ACCOUNTS)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Param 'user_id' is empty"));
    }

    @DisplayName("Create new account: user id is not a number")
    @Test
    void testNewAccountUserIdNotNumber() {
        given()
                .param(Server.PARAM_USER_ID, "t")
                .param(Server.PARAM_CURRENCY, CURRENCY1)

                .when()
                .post(Endpoints.ENDPOINT_ACCOUNTS)

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 't', should be a number"));
    }

    @DisplayName("Create new account: user id not found")
    @Test
    void testNewAccountUserIdNotFound() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);

        given()
                .param(Server.PARAM_USER_ID, 1)
                .param(Server.PARAM_CURRENCY, CURRENCY1)

                .when()
                .post(Endpoints.ENDPOINT_ACCOUNTS)

                .then()
                .statusCode(HttpStatus.NOT_FOUND_404)
                .body(equalTo("User with id 1 not found"));
    }

    @DisplayName("Test get account")
    @Test
    void testGetAccount() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);

        checkGetAccount404(0);
        checkGetAccount404(1);

        checkSuccessfulCreateAccount(0, CURRENCY1, 0);

        checkGetAccount(0, CURRENCY1, 0);
        checkGetAccount404(1);
        checkGetUser(0, NAME1, PHONE1, Collections.singletonList(
                Arrays.asList("0", CURRENCY1, "0")
        ));

        checkSuccessfulCreateAccount(0, CURRENCY2, 1);

        checkGetAccount(0, CURRENCY1, 0);
        checkGetAccount(1, CURRENCY2, 0);
        checkGetUser(0, NAME1, PHONE1, Arrays.asList(
                Arrays.asList("0", CURRENCY1, "0"),
                Arrays.asList("1", CURRENCY2, "0")
        ));
    }

    @DisplayName("Test get account: account id not number")
    @Test
    void testGetAccountIdNotNumber() {
        given()
                .when()
                .get(Endpoints.ENDPOINT_ACCOUNT.replace(Endpoints.ID_PARAM, "t"))

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 't', should be a number"));
    }

    @DisplayName("Test delete account")
    @Test
    void testDeleteAccount() {
        checkDeleteAccount404(0);
        checkDeleteAccount404(1);

        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY2, 1);

        checkGetAccount(0, CURRENCY1, 0);
        checkGetAccount(1, CURRENCY2, 0);
        checkGetUser(0, NAME1, PHONE1, Arrays.asList(
                Arrays.asList("0", CURRENCY1, "0"),
                Arrays.asList("1", CURRENCY2, "0")
        ));

        checkDeleteAccount(0);

        checkGetAccount404(0);
        checkGetAccount(1, CURRENCY2, 0);
        checkGetUser(0, NAME1, PHONE1, Collections.singletonList(
                Arrays.asList("1", CURRENCY2, "0")
        ));

        checkDeleteAccount404(0);
        checkDeleteAccount(1);

        checkGetAccount404(0);
        checkGetAccount404(1);
        checkGetUser(0, NAME1, PHONE1);
    }

    @DisplayName("Test delete account: account id not number")
    @Test
    void testDeleteAccountIdNotNumber() {
        given()
                .when()
                .delete(Endpoints.ENDPOINT_ACCOUNT.replace(Endpoints.ID_PARAM, "t"))

                .then()
                .statusCode(HttpStatus.BAD_REQUEST_400)
                .body(equalTo("Wrong value 't', should be a number"));
    }

    @DisplayName("Test that deleting user also deletes accounts")
    @Test
    void testDeleteUserAlsoDeletesAccounts() {
        checkSuccessfulCreateUser(NAME1, PHONE1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY1, 0);
        checkSuccessfulCreateAccount(0, CURRENCY2, 1);

        checkGetAccount(0, CURRENCY1, 0);
        checkGetAccount(1, CURRENCY2, 0);
        checkGetUser(0, NAME1, PHONE1, Arrays.asList(
                Arrays.asList("0", CURRENCY1, "0"),
                Arrays.asList("1", CURRENCY2, "0")
        ));

        checkDeleteUser(0);

        checkGetUser404(0);
        checkGetAccount404(0);
        checkGetAccount404(1);
    }

}
