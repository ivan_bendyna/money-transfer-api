# Money transfer API
Run: java Application <port> <countThreads>

use 0 for random available port

Example: java Application 8080 8

#### Create user
POST /v1/users?name=<name>&phone=<phone>

Name and phone could be any non-empty strings

201 - account created, user info in body
```
{
    "id": 0,
    "name": "Ivan",
    "phone": " 375331112222",
    "accounts": []
}
```

#### Get user
GET /v1/users/<user_id>

200 - OK, user info in body
```
{
    "id": 0,
    "name": "Ivan",
    "phone": " 375331112222",
    "accounts": [
        {
            "id": 0,
            "currency": "EUR",
            "balance": 0
        }
    ]
}
```
404 - user not found

#### Delete user
GET /v1/users/<user_id>

200 - user deleted

404 - user not found

#### Create account
POST /v1/accounts?user_id=<user_id>&currency=<currency>

Currency can be any non-empty string

201 - account created, account info in body
```
{
    "id": 0,
    "currency": "EUR",
    "balance": 0
}
```
404 - user not found

#### Get account
GET /v1/accounts/<account_id>

200 - OK, account info in body
```
{
    "id": 0,
    "currency": "EUR",
    "balance": 0
}
```
404 - account not found

#### Delete account
GET /v1/accounts/<account_id>

200 - account deleted

404 - account not found

#### Deposit
POST /v1/deposit?account_id=<account_id>&amount=<amount>

Amount should be a positive number

200 - OK, deposit is successful

404 - account not found

#### Transfer
POST /v1/transfer?source_account_id=<source_account_id>&dest_account_id=<dest_account_id>&amount=<amount>

200 - OK, transfer is successful

404 - source or destination account not found

400 - source and destination are the same or their currency is different or source account does not have enough money
